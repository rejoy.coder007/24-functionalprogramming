var gulp        = require('gulp');
var browserSync = require('browser-sync');

gulp.task('browser-sync', function() {
    browserSync({
        files: ["public/**/*.*"],
        port : 3200,
        server: {
            baseDir: "public" // Change this to your web root dir
        }
    });
});

// Default task to be run with `gulp`
gulp.task('default', ["browser-sync"]);